﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportsApp.Models
{
    public class ReportListModel
    {
        
        public ReportListModel()
        {
            Reports = new List<ReportModel>();
        }
        /// <summary>
        /// The Base 64 representation of the logo that should be used.
        /// </summary>
        public string LogoBase64String { get; set; }

        /// <summary>
        /// Image resolution in the PDF report
        /// </summary>
        public int ImageDpi { get; set; }

        /// <summary>
        /// The language the PDF should be generated in
        /// </summary>
        public string Culture { get; set; }

        public List<ReportModel> Reports { get; set; }
        
        public string  ReportTitle { get; set; }

        public string ApiKey { get; set; }

        public string ApiPassword { get; set; }

        public List<ExcelColumnEmptyNumber> excelColumnEmptyNumbers { get; set; }


    }
}