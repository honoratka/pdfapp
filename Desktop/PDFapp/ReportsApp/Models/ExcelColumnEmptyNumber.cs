﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportsApp.Models
{
    public class ExcelColumnEmptyNumber
    {       
        public int ColumnNumber { get; set; }

        public bool IsColumnEmpty { get; set; }

        public bool IsHidden { get; set; }
    }
}