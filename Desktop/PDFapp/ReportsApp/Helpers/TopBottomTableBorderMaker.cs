﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportsApp.Helpers
{
   
    class TopBottomTableBorderMaker : IPdfPTableEvent
    {
        private BaseColor _borderColor;
        private float _borderWidth;
        private PdfWriter _writer;
        /// <summary>
        /// Add a top and bottom border to the table.
        /// </summary>
        /// <param name="borderColor">The color of the border.</param>
        /// <param name="borderWidth">The width of the border</param>
        public TopBottomTableBorderMaker(PdfWriter writer, BaseColor borderColor, float borderWidth)
        {
            this._borderColor = borderColor;
            this._borderWidth = borderWidth;
            this._writer = writer;
        }
        public void TableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases)
        {
            //widths (should be thought of as x's) is an array of arrays, first index is for each row, second index is for each column
            //The below uses first and last to calculate where each X should start and end
            var firstRowWidths = widths[0];
            var lastRowWidths = widths[widths.Length - 1];

            var firstRowXStart = firstRowWidths[0];
            var firstRowXEnd = firstRowWidths[firstRowWidths.Length - 1] - firstRowXStart;

            var lastRowXStart = lastRowWidths[0];
            var lastRowXEnd = lastRowWidths[lastRowWidths.Length - 1] - lastRowXStart;

            //heights (should be thought of as y's) is the y for each row's top plus one extra for the last row's bottom
            //The below uses first and last to calculate where each Y should start and end
            var firstRowYStart = heights[0];
            var firstRowYEnd = heights[1] - firstRowYStart;

            var lastRowYStart = heights[heights.Length - 1];
            var lastRowYEnd = heights[heights.Length - 2] - lastRowYStart;

            //Where we're going to draw our lines
            PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];

            //I always try to save the previous state before changinge anything
            canvas.SaveState();

            //Set our line properties
            canvas.SetLineWidth(this._borderWidth);
            canvas.SetColorStroke(this._borderColor);

            ////Draw some rectangles firstRowYStart
            //canvas.Rectangle(
            //                firstRowXStart,
            //                lastRowYStart,
            //                lastRowXEnd,
            //                firstRowYStart - lastRowYStart
            //                );

            //canvas.Stroke();
            DrawRoundedShadedRectangle(this._writer, canvas, firstRowXStart, lastRowYStart, lastRowXEnd, firstRowYStart - lastRowYStart, 20, 0, BaseColor.WHITE, this._borderColor, this._borderColor);

            //They aren't actually drawn until you stroke them!


            //Restore any previous settings
            canvas.RestoreState();

        }

        void DrawRoundedShadedRectangle(PdfWriter writer, PdfContentByte canvas, float x, float y, float w, float h, float r, float shade, BaseColor innerColor, BaseColor shadeColor, BaseColor outerColor)
        {
            //PdfContentByte canvas = writer.DirectContent;

            canvas.SaveState();
            canvas.Rectangle(x - shade, y + r, w + 2 * shade, h - 2 * r);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingRight = PdfShading.SimpleAxial(writer, x + w, y, x + w + shade, y, shadeColor, outerColor, false, false);
            canvas.PaintShading(shadingRight);
            PdfShading shadingLeft = PdfShading.SimpleAxial(writer, x, y, x - shade, y, shadeColor, outerColor, false, false);
            canvas.PaintShading(shadingLeft);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.Rectangle(x + r, y - shade, w - 2 * r, h + 2 * shade);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingTop = PdfShading.SimpleAxial(writer, x, y + h, x, y + h + shade, shadeColor, outerColor, false, false);
            canvas.PaintShading(shadingTop);
            PdfShading shadingBottom = PdfShading.SimpleAxial(writer, x, y, x, y - shade, shadeColor, outerColor, false, false);
            canvas.PaintShading(shadingBottom);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.Rectangle(x + w - r, y + h - r, r + shade, r + shade);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingTopRight = PdfShading.SimpleRadial(writer, x + w - r, y + h - r, r, x + w - r, y + h - r, r + shade, shadeColor, outerColor);
            canvas.PaintShading(shadingTopRight);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.Rectangle(x - shade, y + h - r, r + shade, r + shade);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingTopLeft = PdfShading.SimpleRadial(writer, x + r, y + h - r, r, x + r, y + h - r, r + shade, shadeColor, outerColor);
            canvas.PaintShading(shadingTopLeft);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.Rectangle(x - shade, y - shade, r + shade, r + shade);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingBottomLeft = PdfShading.SimpleRadial(writer, x + r, y + r, r, x + r, y + r, r + shade, shadeColor, outerColor);
            canvas.PaintShading(shadingBottomLeft);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.Rectangle(x + w - r, y - shade, r + shade, r + shade);
            canvas.Clip();
            canvas.NewPath();
            PdfShading shadingBottomRight = PdfShading.SimpleRadial(writer, x + w - r, y + r, r, x + w - r, y + r, r + shade, shadeColor, outerColor);
            canvas.PaintShading(shadingBottomRight);
            canvas.RestoreState();

            canvas.SaveState();
            canvas.SetColorFill(innerColor);
            canvas.SetColorStroke(innerColor);
            canvas.RoundRectangle(x, y, w, h, r);
            canvas.FillStroke();
            canvas.RestoreState();
        }
    }
}