﻿using System;
using System.Collections.Generic;
using System.Linq;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using ReportsApp.Models;
using System.Web.Http;
using System.Diagnostics;
using System.Drawing;
using System.Resources;
using System.Globalization;
using ImageProcessor;
using Image = System.Drawing.Image;
using ImageProcessor.Imaging;
using System.Drawing.Imaging;
using System.Configuration;
using iTextSharp.text.pdf.draw;

namespace iTextSharp.simple.Part1
{
    public class PDFController : ApiController
    {
        public ResourceManager rm = null;

        //constructor - should handle injection - Unity
        public PDFController()
        {
            rm = new ResourceManager("ReportsApp.Resources.LanguageResxFiles.pagedialogs", System.Reflection.Assembly.GetExecutingAssembly());
        }

        // [System.Web.Http.HttpPost]
        //[Authorize]
        //[AcceptVerbs(HttpVerbs.Post)]
        public async Task<IHttpActionResult> Post(ReportListModel models)
        {
            //simple checking ApiKey & ApiPassword
            string ApiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
            string ApiPassword = System.Configuration.ConfigurationManager.AppSettings["ApiPassword"];

            if (models.ApiKey != ApiKey) return Json( new Rsponse { message = "Wrong ApiKey", result = false });
            if (models.ApiPassword != ApiPassword) return Json(new Rsponse { message = "Wrong ApiPassword", result = false });

            //Correction: The incoming model should hold preferences, logo, etc. Use ReportListModel instead. I wrote this class for you...
            //There should be two different end points as well. 
            //api/Report & api/ReportList        

            var modelState = ModelState.Values;

                foreach (var m in models.Reports)
                {
                    //Correction: eh... when about m here. You are iterating but not testing m.. Makes no sense this loop TODO
                    if (!ModelState.IsValid)
                    {
                        return Json(new { result = false, message = ModelState.Values.SelectMany(n => n.Errors) });
                    }
                }
                //Json response
                Rsponse rsponse = new Rsponse();

                 //Correction: Culture is hard coded. Should come from the model!
                CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(models.Culture); 
            
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (Document document = new Document(PageSize.A4, 40, 25, 30, 30))
                        {
                        BaseFont bf = BaseFont.CreateFont("ARIALUNI.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, false, ReportsApp.Resources.Fonts.Fonts.ARIALUNI, null);
                        text.Font titleFont = new text.Font(bf, 14, iTextSharp.text.Font.BOLD);

                        text.Font boldTableFont = new text.Font(bf, 10, iTextSharp.text.Font.BOLD);
                        text.Font normalTableFont = new text.Font(bf, 10, iTextSharp.text.Font.NORMAL);

                        using (PdfWriter writer = PdfWriter.GetInstance(document, ms))
                        {
                            document.Open();

                            //Correction: Logo should come from the model. Each reporting solution has their own logo. DONE
                            var imgLogo = iTextSharp.text.Image.GetInstance(await RenderLogolocal(models.LogoBase64String));
                            imgLogo.WidthPercentage = 15;
                            imgLogo.SetDpi(200, 200);
                            imgLogo.ScaleToFit(48f, 48f);
                            document.Add(imgLogo);
                            
                            // Load font from resources file
                                            

                            var headParlabel = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReportGeneric", new CultureInfo(models.Culture));
                            var headPar = new Paragraph(headParlabel + ": " + models.ReportTitle, titleFont);                          

                            document.Add(headPar);

                            LineSeparator line = new LineSeparator(3f, 100f, new BaseColor(220, 127, 49), Element.ALIGN_LEFT, 1);
                            document.Add(new Chunk(line));                         

                            // preparing for gets infotables from Parallel threads
                            List< InfotableModelHelper> InfotableList = new List<InfotableModelHelper>();

                            // Now program is strongly prepared for Parallel computing, each raport is in other process where each Image from File from raport has own render proces. 
                               Parallel.ForEach(models.Reports, (m) =>
                               {
                                   var infotable = (new PdfPTable(12)
                                   {
                                       WidthPercentage = 100,
                                       HorizontalAlignment = 0,
                                       SpacingBefore = 10,
                                       SpacingAfter = 10
                                   });
                                   infotable.DefaultCell.Border = 0;
                                   
                                  
                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       PaddingBottom = 2,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       Phrase = new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "IdGeneric", new CultureInfo(models.Culture)), boldTableFont)
                                   });

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Phrase = new Phrase(m.Id.ToString(), normalTableFont),
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Colspan = 10
                                   });

                                   //created date
                                   var labelTextCDate = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CreatedDateGeneric", new CultureInfo(models.Culture));
                                   var TextCDate = ReportsApp.Helpers.Helpers.DateLanguageConversion(m.CreatedDateTime.ToLocalTime(), new CultureInfo(models.Culture)) + $"{m.CreatedDateTime.ToLocalTime(): HH:mm}";
                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(labelTextCDate, boldTableFont)
                                   });

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Phrase = new Phrase(TextCDate, normalTableFont),
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Colspan = 10
                                   });

                                   //start date
                                   var labelTextSDate = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "OccurrenceDateLabel", new CultureInfo(models.Culture));
                                   var TextSDate = ReportsApp.Helpers.Helpers.DateLanguageConversion(m.StartDate.ToLocalTime(), new CultureInfo(models.Culture)) + $"{m.StartDate.ToLocalTime(): HH:mm}";

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(labelTextSDate, boldTableFont)
                                   });

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Phrase = new Phrase(TextSDate, normalTableFont),
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Colspan = 10
                                   });

                                   if (m.StartDate != m.EndDate)
                                   {
                                       var labelTextEDate = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "EndDateLabel", new CultureInfo(models.Culture));
                                       var TextEDate = ReportsApp.Helpers.Helpers.DateLanguageConversion(m.EndDate.ToLocalTime(), new CultureInfo(models.Culture)) + $"{m.EndDate.ToLocalTime(): HH:mm}";

                                       infotable.AddCell(new PdfPCell
                                       {
                                           Colspan = 2,
                                           Border = 0,
                                           BorderWidthBottom = 0.2f,
                                           BorderColorBottom = BaseColor.LIGHT_GRAY,
                                           PaddingBottom = 2,
                                           Phrase = new Phrase(labelTextEDate, boldTableFont)
                                       });

                                       infotable.AddCell(new PdfPCell
                                       {
                                           Phrase = new Phrase(TextEDate, normalTableFont),
                                           Border = 0,
                                           BorderWidthBottom = 0.2f,
                                           BorderColorBottom = BaseColor.LIGHT_GRAY,
                                           PaddingBottom = 2,
                                           Colspan = 10
                                       });
                                   }

                                   //who created the report
                                   var labelAuthor = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReportedByLabel", new CultureInfo(models.Culture));

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(labelAuthor, boldTableFont)
                                   });
                                   infotable.AddCell(new PdfPCell
                                   {
                                       Phrase = new Phrase(m.CreatedByName, normalTableFont),
                                       Colspan = 10,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2
                                   });

                                   //dimension name and parent path
                                   var labelDim = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DimensionGeneric", new CultureInfo(models.Culture));
                                   var NotAssigned = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "NotAssigned", new CultureInfo(models.Culture));

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       Phrase = new Phrase(labelDim, boldTableFont)
                                   });
                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 10,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(
                                           !string.IsNullOrEmpty(m.ParentDimensionSequenceString)
                                               ? m.ParentDimensionSequenceString + " " + m.DimensionName
                                               : !string.IsNullOrEmpty(m.DimensionName)
                                                   ? m.DimensionName
                                                   : NotAssigned, normalTableFont
                                       )
                                   });

                                   //handler list part..
                                   var labelHandlers = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "HandlersGeneric", new CultureInfo(models.Culture));

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderWidthBottom = 0.2f,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(labelHandlers, boldTableFont)
                                   });

                                   if (m.Handlers.Any())
                                   {
                                       var handlerTable = new PdfPTable(m.Handlers.Count)
                                       {
                                           WidthPercentage = 100,
                                           HorizontalAlignment = 0,
                                           SpacingBefore = 0,
                                           SpacingAfter = 10
                                       };

                                       handlerTable.DefaultCell.Border = 0;
                                       foreach (var str in m.Handlers)
                                       {
                                           handlerTable.AddCell(new PdfPCell
                                           {
                                               Phrase = new Phrase(str, normalTableFont),
                                               Border = 0
                                           });
                                       }

                                       var handlerTableHolder = new PdfPCell
                                       {
                                           Colspan = 10,
                                           Border = 0,
                                           PaddingBottom = 2,
                                           BorderWidthBottom = 0.2f,
                                           BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       };
                                       handlerTableHolder.AddElement(handlerTable);
                                       infotable.AddCell(handlerTableHolder);
                                   }

                                   //end handler list
                                   //status
                                   var labelstatus = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusGeneric", new CultureInfo(models.Culture));

                                   infotable.AddCell(new PdfPCell
                                   {
                                       Colspan = 2,
                                       Border = 0,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       BorderWidthBottom = 0.2f,
                                       PaddingBottom = 2,
                                       Phrase = new Phrase(labelstatus, boldTableFont)
                                   });
                                   infotable.AddCell(new PdfPCell
                                   {
                                       Phrase = new Phrase(GetStatusDescription(m.Status, new CultureInfo(models.Culture)), boldTableFont),
                                       Colspan = 10,
                                       Border = 0,
                                       BorderColorBottom = BaseColor.LIGHT_GRAY,
                                       BorderWidthBottom = 0.2f,
                                       PaddingBottom = 2
                                   });

                                   var completedDateLabel = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CompletedDate", new CultureInfo(models.Culture));
                                   //completion date
                                   if (m.Status == ReportStatus.Completed)
                                   {

                                       var completedJournalEntry = m.entries;
                                       if (completedJournalEntry.Any())
                                       {
                                           var dtCompleted = completedJournalEntry.First().DateTime.ToLocalTime();
                                           infotable.AddCell(new PdfPCell
                                           {
                                               Colspan = 2,
                                               Border = 0,
                                               BorderWidthBottom = 0.2f,
                                               BorderColorBottom = BaseColor.LIGHT_GRAY,
                                               Phrase = new Phrase(completedDateLabel, boldTableFont)
                                           });
                                           infotable.AddCell(new PdfPCell
                                           {
                                               Phrase = new Phrase($"{dtCompleted:dd. MMM yyyy HH:mm}", normalTableFont),
                                               Colspan = 10,
                                               Border = 0,
                                               BorderWidthBottom = 0.2f,
                                               BorderColorBottom = BaseColor.LIGHT_GRAY
                                           });
                                       }
                                   }

                                   //descriptions
                                   foreach (var description in m.Descriptions)
                                   {
                                       infotable.AddCell(new PdfPCell
                                       {
                                           Colspan = 2,
                                           Border = 0,
                                           BorderWidthBottom = 0.2f,
                                           BorderColorBottom = BaseColor.LIGHT_GRAY,
                                           Phrase = new Phrase(GetDescriptionTypeLabel(description.Type, new CultureInfo(models.Culture)), boldTableFont)
                                       });
                                       infotable.AddCell(new PdfPCell
                                       {
                                           Phrase = new Phrase(description.Description, normalTableFont),
                                           Colspan = 10,
                                           Border = 0,
                                           BorderWidthBottom = 0.2f,
                                           BorderColorBottom = BaseColor.LIGHT_GRAY
                                       });
                                   }

                                

                                   var validExtensions = new List<string> { ".jpg", ".png", ".jpeg" };

                                   //find picture attachments
                                   List<ReportFile> files = m.Files.Where(z =>
                                   {
                                       var extension = Path.GetExtension(z.Filename);
                                       return extension != null && validExtensions.Contains(extension.ToLower());
                                   }).ToList();

                                   var imageColumns = 2;
                                   var configColumns = m.ConfigColumns;
                                   if (!string.IsNullOrEmpty(configColumns))
                                   {
                                       imageColumns = Convert.ToInt32(configColumns);
                                   }

                                   var photoCell = new PdfPCell { Border = 0, Colspan = 12 };
                                   var imageTable = new PdfPTable(imageColumns)
                                   {
                                       WidthPercentage = 100,
                                       HorizontalAlignment = 0,
                                       SpacingBefore = 0,
                                       SpacingAfter = 0
                                   };
                                   var tempMem = files.Count;
                                   var leftOverCells = 0;
                                   if (tempMem > imageColumns)
                                   {
                                       leftOverCells = tempMem % imageColumns;
                                   }
                                   if (tempMem < imageColumns)
                                   {
                                       leftOverCells = imageColumns - tempMem;
                                   }
                                                                      
                                   if (files.Any())
                                   {
                                       // Each picture render in other process
                                       Parallel.ForEach(files, (reportile) =>
                                       {
                                           var imgProcess = new ImageFactory(true);
                                           using (var stream = new MemoryStream())
                                           {
                                               var imgBytes =  Convert.FromBase64String(reportile.Bytes);
                                               stream.Read(imgBytes , 0, (int)stream.Length);
                                               using (var outstream = new MemoryStream())
                                               {
                                                   var img = imgProcess.Load(imgBytes);
                                                   img.Resize(new ResizeLayer(new Size(1280, 1280), ResizeMode.Max));
                                                   img.Save(outstream);

                                                   var cPhotoCell = new PdfPCell
                                                   {
                                                       Border = 0
                                                   };

                                                   outstream.Position = 0;
                                                   var pfBmp = iTextSharp.text.Image.GetInstance(Image.FromStream(outstream), ImageFormat.Jpeg);
                                                   pfBmp.SetDpi(models.ImageDpi, models.ImageDpi);
                                                   cPhotoCell.AddElement(pfBmp);
                                                   imageTable.AddCell(cPhotoCell);
                                               }
                                           }
                                       // end of image Parallel
                                       });

                                       if (leftOverCells > 0)
                                       {
                                           for (var i = 0; i < leftOverCells; i++)
                                           {
                                               imageTable.AddCell(new PdfPCell
                                               {
                                                   Border = 0
                                               });
                                           }
                                       }

                                       try
                                       {
                                           photoCell.AddElement(imageTable);
                                           infotable.AddCell(photoCell);
                                       }
#pragma warning disable 168
                                       catch (OutOfMemoryException ex)
                                       {
                                           //Debug.Print(ex.Message);
                                       }
                                       catch (Exception ex)
                                       {
                                           //Debug.Print(ex.Message);
                                       }
#pragma warning restore 168
                                   }

                                   var correctiveActionsLabel = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CorrectiveActionsLabel", new CultureInfo(models.Culture));

                                   if (m.CorrectiveActions.Any())
                                   {
                                       foreach (var action in m.CorrectiveActions)
                                       {
                                           var corrActionHeaderCell = new PdfPCell
                                           {
                                               Colspan = 12,
                                               Border = 0,
                                               BorderWidthBottom = 0.2f,
                                               BorderColorBottom = BaseColor.LIGHT_GRAY,
                                               PaddingTop = 24
                                           };
                                           corrActionHeaderCell.AddElement(new Phrase(correctiveActionsLabel, boldTableFont));
                                           infotable.AddCell(corrActionHeaderCell);

                                           var actionCell = new PdfPCell
                                           {
                                               Colspan = 12,
                                               Border = 0,
                                               BorderWidthBottom = 0.2f,
                                               BorderColorBottom = BaseColor.LIGHT_GRAY,
                                               Padding = 0
                                           };
                                           var users = new List<string>();

                                           if (action.AssignedTo.Any())
                                           {
                                               foreach (var usr in action.AssignedTo)
                                               {
                                                   users.Add(usr);
                                               }
                                           }


                                           if (users.Any())
                                           {
                                               actionCell.AddElement(new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "AssignedTo", new CultureInfo(models.Culture)) + ": " + string.Join(", ", users), normalTableFont));
                                           }
                                           actionCell.AddElement(new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CreatedDateGeneric", new CultureInfo(models.Culture)) + ": " + $"{action.CreateDateTime:dd. MMM yyyy HH:mm}", normalTableFont));
                                           if (action.ExpectedImplementationDate != null)
                                           {
                                               actionCell.AddElement(new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DesiredImplementationDate", new CultureInfo(models.Culture)) + ": " + $"{action.ExpectedImplementationDate:dd. MMM yyyy HH:mm}", normalTableFont));
                                           }

                                           actionCell.AddElement(new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "IsTheCorrectiveActionPerformed", new CultureInfo(models.Culture)) + " " + (action.ImplementationDegree != 0 ? ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "YesGeneric", new CultureInfo(models.Culture)) : ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "NoGeneric", new CultureInfo(models.Culture))), normalTableFont));

                                           actionCell.AddElement(new Phrase(action.Description, normalTableFont));
                                           if (!string.IsNullOrEmpty(action.ResultComment))
                                           {
                                               actionCell.AddElement(new Phrase(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CommentsGeneric", new CultureInfo(models.Culture)), boldTableFont));
                                               actionCell.AddElement(new Phrase(action.ResultComment, normalTableFont));
                                           }
                                           infotable.AddCell(actionCell);
                                       }
                                   }

                                   //CommentsGeneric
                                   var commentsGenericLabel = ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CommentsGeneric", new CultureInfo(models.Culture));
                                                                     
                                   if (m.comments.Any())
                                   {
                                       var commentsHeaderCell = new PdfPCell
                                       {
                                           Colspan = 12,
                                           Border = 0,
                                           Padding = 0,
                                           PaddingTop = 24
                                       };
                                       commentsHeaderCell.AddElement(new Phrase(commentsGenericLabel, boldTableFont));
                                       infotable.AddCell(commentsHeaderCell);
                                       foreach (var c in m.comments)
                                       {
                                           var commentCell = new PdfPCell
                                           {
                                               Colspan = 12,
                                               Border = 0,
                                               BorderWidthBottom = 0.2f,
                                               BorderColorBottom = BaseColor.LIGHT_GRAY,
                                               PaddingTop = 4
                                           };
                                           var commentTable = new PdfPTable(6)
                                           {
                                               WidthPercentage = 100,
                                               HorizontalAlignment = 0,
                                               SpacingBefore = 0,
                                               SpacingAfter = 10
                                           };
                                           var createdDateCell = new PdfPCell
                                           {
                                               Border = 0,
                                               PaddingTop = 4
                                           };
                                           createdDateCell.AddElement(
                                               new Phrase(c.DateTimeCreated.ToString("dd MMM yyyy HH:mm"), normalTableFont));

                                           var createdByCell = new PdfPCell
                                           {
                                               Border = 0,
                                               PaddingTop = 4
                                           };
                                           createdByCell.AddElement(new Phrase(c.CreatedBy, normalTableFont));

                                           var commentTextCell = new PdfPCell
                                           {
                                               Border = 0,
                                               PaddingTop = 4,
                                               Colspan = 6
                                           };
                                           commentTextCell.AddElement(new Phrase(c.CommentText, normalTableFont));

                                           commentTable.AddCell(createdDateCell);
                                           commentTable.AddCell(createdByCell);
                                           commentTable.AddCell(commentTextCell);

                                           commentCell.AddElement(commentTable);
                                           infotable.AddCell(commentCell);
                                       }
                                   }

                                   // add infotable to InfotableList, we don't know order of this
                                   InfotableList.Add(new InfotableModelHelper
                                   {
                                       Infotable = infotable,
                                       Id = m.Id,
                                       DateTime = m.DateTimeCreated,
                                   });
                                   //end of Parallel 
                               });

                               try
                               {
                               // In one process all generated PDFTables are add to documnet, it is possible to change order (for testing by ID or DateTimeCreated )
                                    foreach (var element in InfotableList.OrderBy(o => o.Id ))
                                    {
                                    Random randonGen = new Random();
                                    BaseColor randomColor = new BaseColor(randonGen.Next(255), randonGen.Next(255),
                                    randonGen.Next(255));

                                    element.Infotable.TableEvent = new ReportsApp.Helpers.TopBottomTableBorderMaker(writer,randomColor, 2f);

                                    document.Add(element.Infotable);
                                    }

                               }

                                

                                catch (BadElementException ex)
                                {
                                    Debug.Print(ex.Message);
                                }
                                catch (DocumentException ex)
                                {
                                    Debug.Print(ex.Message);
                                }
                                catch (OutOfMemoryException ex)
                                {
                                    Debug.Print(ex.Message);
                                }
                                catch (Exception ex)
                                {
                                    Debug.Print(ex.Message);
                                }

                                
                                writer.CloseStream = false;
                                document.Close();
                            }


                           
                         

                        //convert memorystream to byte array         
                        ms.CopyTo(ms);
                        byte[] msAsByte = ms.ToArray();
                        ms.Close();

                       
                        //Adding number of pages             
                        using (MemoryStream stream = new MemoryStream())
                        {
                            PdfReader reader = new PdfReader(msAsByte);                        
                            using (PdfStamper stamper = new PdfStamper(reader, stream))
                            {
                                int pages = reader.NumberOfPages;
                                for (int i = 1; i <= pages; i++)
                                {
                                    ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_LEFT, new Phrase( String.Format("{0} / {1}", i, pages), normalTableFont), 15f, 15f, 0);
                                }
                            }
                            msAsByte = stream.ToArray();
                        }

                        //  convert byte array to base64
                        var msAsBase64Byte = Convert.ToBase64String(msAsByte);

                            rsponse = new Rsponse
                            {
                                result = true,
                                message = msAsBase64Byte,
                                id = "4",
                            };                            
                        }
                    }
                }
                catch (DocumentException de)
                {
                    throw de;
                }

                catch (IOException ioe)
                {
                    throw ioe;
                }
        return Json(rsponse);       
        }


        private async Task<byte[]> RenderLogolocal(string LogoBase64String)
        {
            //wrapping it all in a task. If not the async statement has no sense
            return await Task.Run(() =>
            {
                var files = LogoBase64String;
                if (files.Any())
                {
                    using (var stream = new MemoryStream())
                    {
                        var bytes = ReportsApp.Helpers.Helpers.imageToByteArray((Image)ReportsApp.Resources.Images.Images.logo_oa);
                        stream.Read(bytes, 0, (int)stream.Length);

                        using (var filestream = new MemoryStream(bytes))
                        {
                            using (var outStream = new MemoryStream())
                            {
                                using (var imageFactory = new ImageFactory(true))
                                {

                                    var fact = imageFactory.Load(filestream);
                                    fact.Resize(new ResizeLayer(new Size(320, 320), ResizeMode.Max));
                                    fact.Save(outStream);
                                    outStream.Position = 0;
                                    return outStream.ToArray();
                                }
                            }
                        }
                    }
                }

                var oBitmap = new Bitmap(180, 60);
                var oGraphic = Graphics.FromImage(oBitmap);
                var oColor = Color.Black;
                var oBrush = new SolidBrush(oColor);
                oGraphic.FillRectangle(oBrush, 0, 0, 180, 60);
                var oBrushWrite = new SolidBrush(Color.White);
                var oFont = new System.Drawing.Font("Arial", 13);
                var oPoint = new PointF(8f, 8f);
                oGraphic.DrawString("No logo defined", oFont, oBrushWrite, oPoint);

                // Response.ContentType = "image/png";

                //better to use stream like this. It is done above, so why not here?
                using (var str = new MemoryStream())
                {
                    oBitmap.Save(str, ImageFormat.Png);
                    return str.GetBuffer();
                }                
            });           
        }
               

        private string GetStatusDescription(ReportStatus status, CultureInfo cultureInfo)
        {
            switch (status)
            {
                case ReportStatus.Created:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCreated", cultureInfo);
                case ReportStatus.Read:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCreated", cultureInfo);
                case ReportStatus.ReadByHandler:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusReadByHandler", cultureInfo);
                case ReportStatus.HandlingStarted:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusHandlingStarted", cultureInfo);
                case ReportStatus.OnHold:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCreated", cultureInfo);
                case ReportStatus.Forwarded:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusForwarded", cultureInfo);
                case ReportStatus.CorrectiveActionStarted:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCorrectiveActionStarted", cultureInfo);
                case ReportStatus.CorrectiveActionCompletedPendingResult:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCorrectiveActionCompletedPendingResult", cultureInfo);
                case ReportStatus.Completed:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm,"StatusCompleted", cultureInfo);
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }

        private string GetDescriptionTypeLabel(DescriptionType type, CultureInfo cultureInfo)
        {
            switch (type)
            {
                case DescriptionType.General:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "DescriptionLabelGeneric", cultureInfo);
                case DescriptionType.Situation:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "SituationDescriptionLabel", cultureInfo);
                case DescriptionType.Reason:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "ReasonDescriptionLabel", cultureInfo);
                case DescriptionType.Consequence:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "ConsequenceDescriptionLabel", cultureInfo);
                case DescriptionType.ActionsTaken:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "ActionsTakenDescriptionLabel", cultureInfo);
                case DescriptionType.Solution:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion( rm, "SolutionDescriptionLabel", cultureInfo);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}